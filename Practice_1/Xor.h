#pragma once
#include <string>

class Xor
{
public:
	Xor() = delete;

	explicit Xor(const std::string& key);
	~Xor();

	std::string Encrypt(const std::string& data);
	std::string Decrypt(const std::string& data);

private:
	std::string mKey;
};

