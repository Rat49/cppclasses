#include "Xor.h"
#include <cassert>

namespace
{
	std::string Cipher(const std::string& key, const std::string& data)
	{
		std::string result(data);

		std::string::const_iterator keyBegin = key.cbegin();
		std::string::const_iterator keyEnd = key.cend();
		std::string::const_iterator keyIt = keyBegin;

		for (std::string::iterator resultIt = result.begin(), resultEnd = result.end();
			resultIt != resultEnd;
			++resultIt)
		{
			*resultIt = *resultIt ^ *keyIt;
			//*resultIt ^= *keyIt;

			++keyIt;
			if (keyIt == keyEnd)
			{
				keyIt = keyBegin;
			}
		}
		return result;
	}
}

Xor::Xor(const std::string& key)
	: mKey(key)
{
	assert(!mKey.empty() && "Key should not be empty");
}

Xor::~Xor()
{
}

std::string Xor::Encrypt(const std::string& data)
{
	return Cipher(mKey, data);
}

std::string Xor::Decrypt(const std::string& data)
{
	return Cipher(mKey, data);
}

