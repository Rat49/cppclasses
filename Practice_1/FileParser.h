#pragma once
#include <fstream>
#include <string>
#include <map>

class FileParser
{
public:
	FileParser();
	~FileParser();

	void Add(const std::string& filename);
	void Remove(const std::string& filename, bool save = false);

	std::string GetData(const std::string& filename);
	void SetData(const std::string& filename, const std::string& data);

private:
	std::map<std::string, std::fstream> mFiles;
};

