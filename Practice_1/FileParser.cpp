#include "FileParser.h"
#include <iostream>

FileParser::FileParser()
{
}

FileParser::~FileParser()
{
	for (auto& pair : mFiles)
	{
		pair.second.close();
	}
}

void FileParser::Add(const std::string& filename)
{
	std::fstream& file = mFiles[filename];

	file.open(filename, std::ios::in | std::ios::out);
	if (!file.is_open())
	{
		std::cout << "Error: Can't open file " << filename << "\n";
		mFiles.erase(filename);
	}
}

void FileParser::Remove(const std::string& filename, bool save)
{
	auto it = mFiles.find(filename);
	if (it == mFiles.end())
	{
		return;
	}

	it->second.close();
	mFiles.erase(it);
}

std::string FileParser::GetData(const std::string& filename)
{
	auto it = mFiles.find(filename);
	if (it == mFiles.end())
	{
		return std::string();
	}

	it->second.seekg(std::fstream::beg);
	return std::string(
		std::istreambuf_iterator<char>(it->second),
		std::istreambuf_iterator<char>());
}

void FileParser::SetData(const std::string& filename, const std::string& data)
{
	auto it = mFiles.find(filename);
	if (it == mFiles.end())
	{
		return;
	}

	it->second.seekp(std::fstream::beg);
	it->second.write(data.c_str(), data.size());
}

