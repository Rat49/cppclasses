#include <cassert>
#include <iostream>
#include <array>
#include <string>
#include "Xor.h"
#include "FileParser.h"

void Lesson1()
{
	std::string key;
	std::string data;
	
	std::cout << "Enter the key\n";
	std::cin >> key;
	
	std::cout << "Enter the text\n";
	std::cin >> data;
	
	Xor xorer(key);
	
	std::string encrypted = xorer.Encrypt(data);
	std::string decrypted = xorer.Decrypt(encrypted);
	
	std::cout << "Encrypted text: " << encrypted << "\n";
	std::cout << "Decrypted text: " << decrypted << "\n";
}

namespace
{
	enum CommandId : int
	{
		None = -1,
		InputFileCmd,
		OutputFileCmd,
		KeyCmd
	};

	const std::array<std::string, 3> Commands = { "-i", "-o", "-k" };
}

void Lesson2(int argc, char** argv)
{
	std::string inputFile, outputFile, key;
	CommandId currentCmd = None;
	for (int i = 1; i != argc; ++i)
	{
		const char* arg = argv[i];
		switch (currentCmd)
		{
		case InputFileCmd:
			inputFile = arg;
			break;

		case OutputFileCmd:
			//std::string = const char*
			//outputFile.assign(arg);
			outputFile = arg;
			break;

		case KeyCmd:
			key = arg;
			break;

		case None:
			{
				auto it = std::find(Commands.cbegin(), Commands.cend(), arg);
				if (it != Commands.cend())
				{
					currentCmd = static_cast<CommandId>(std::distance(Commands.cbegin(), it));
					continue;
				}
			}
			break;

		default:
			assert(false && "Not implemented");
			break;
		}
		currentCmd = None;
	}

	assert(!inputFile.empty() && !outputFile.empty() && !key.empty() && "Invalid parameters");

	std::cout << "Parameters:\n";
	std::cout << "Input file: " << inputFile << "\n";
	std::cout << "Output file: " << outputFile << "\n";
	std::cout << "Key: " << key << "\n";

	FileParser file;
	file.Add(inputFile);
	file.Add(outputFile);

	Xor xorer(key);
	file.SetData(outputFile, xorer.Encrypt(file.GetData(inputFile)));
}

int main(int argc, char** argv)
{
	//Lesson1();
	Lesson2(argc, argv);

	system("pause");
    return 0;
}

