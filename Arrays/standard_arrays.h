#include <array>
#include <vector>
#include <cassert>

class StdArray
{
public:
    static constexpr unsigned int ArraySize = 10;

public:
    StdArray()
    {
        m_array.assign(0);
    }

    StdArray(const std::array<int, ArraySize>& source) : m_array(source)
    {
    }

    void SetData(int* source, unsigned int count)
    {
        assert(count <= ArraySize);

        std::copy(source, source + count, m_array.begin());
    }

    void SetData(const std::array<int, ArraySize>& source)
    {
        m_array = source;
    }

    const int* GetData() const
    {
        return m_array.data();
    }

    unsigned int GetCount() const
    {
        return m_array.size(); //or return ArraySize
    }

private:
    std::array<int, ArraySize> m_array;
};

class StdVector
{
public:
    StdVector()
    {
    }

    StdVector(const std::vector<int>& source)
    {
        SetData(source);
    }

    StdVector(int* source, unsigned int count)
    {
        SetData(source, count);
    }

    void SetData(int* source, unsigned int count)
    {
        m_array.assign(source, source + count);
    }

    void SetData(const std::vector<int>& source)
    {
        m_array.assign(source.cbegin(), source.cend());
        // or
        //m_array = source;
    }

    const int* GetData() const
    {
        return &m_array[0];
        //or
        return m_array.data();
    }

    unsigned int GetCount() const
    {
        return m_array.size();
    }

private:
    std::vector<int> m_array;
};
