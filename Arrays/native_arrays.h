#include <memory>
#include <cassert>

class StaticArray
{
public:
    StaticArray()
    {
        unsigned int size_of_element_1 = sizeof(*m_array); //equals 4 -sizeof(int)

        unsigned int size_of_element_2 = sizeof(m_array[0]); //equals 4 - sizeof(int)

        unsigned int size_in_elements = sizeof(m_array); //equals ArraySize

        unsigned int size_in_bytes = size_in_elements * size_of_element_1;

        //initialize using standard function
        memset(m_array, 0, size_in_bytes);

        //initialize using cycle
        for (unsigned int i = 0; i < ArraySize; ++i)
        {
            m_array[i] = 0;
        }
    }

    void SetData(int* source, unsigned int count)
    {
        assert(count <= ArraySize); //assert if user asks to copy more than we can accept

        //copy using standard function
        memcpy(m_array, source, count * sizeof(int));

        //copy using cycle
        for (unsigned int i = 0; i < count; ++i)
        {
            m_array[i] = source[i]; //copy using array syntax

            *(m_array + i) = *(source + i); //copy using pointers
        }

        //more exotic variant
        int* dstIt = m_array;
        int* srcIt = source;
        while (count != 0)
        {
            *dstIt = *srcIt;
            ++dstIt;
            ++srcIt;
            --count;
        }
    }

    const int* GetData() const
    {
        return m_array;
    }

    unsigned int GetCount() const
    {
        return ArraySize;
    }

private:
    static constexpr unsigned int ArraySize = 10;
    int m_array[ArraySize];
};

class DynamicArray
{
public:
    DynamicArray() : m_array(nullptr), m_count(0)
    {
    }

    DynamicArray(int* source, unsigned int count) //custom constructor for accepting array
    {
        assert(count != 0); //no reason to create array on 0 elements

        m_count = count;
        m_array = new int[count];
        memcpy(m_array, source, count * sizeof(int));
    }

    ~DynamicArray() //destructor is required to release resources
    {
        if (m_array != nullptr)
        {
            delete[] m_array;
        }
    }

    void SetData(int* source, unsigned int count)
    {
        if (m_count != count)
        {
            if (m_array != nullptr)
            {
                delete[] m_array;
                m_array = nullptr;
            }

            m_count = count;

            if (m_count == 0)
            {
                return;
            }

            m_array = new int[m_count];
        }

        memcpy(m_array, source, m_count * sizeof(int));
    }

    const int* GetData() const
    {
        return m_array;
    }

    unsigned int GetCount() const
    {
        return m_count;
    }

private:
    unsigned int m_count;
    int* m_array;
};
