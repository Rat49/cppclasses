#include "standard_arrays.h"
#include "native_arrays.h"

void main()
{
    int local_array[15] = { 0 };

    ////////////////////////////////////////

    StaticArray a;
    a.SetData(local_array, 15); //need 10

    ////////////////////////////////////////

    DynamicArray d1;
    d1.SetData(local_array, 15);

    ////////////////////////////////////////

    DynamicArray d2(local_array, 15);

    ////////////////////////////////////////
    ////////////////////////////////////////

    StdArray sa;
    sa.SetData(local_array, 15); //need 10

    ////////////////////////////////////////

    StdVector sv1;
    sv1.SetData(local_array, 15); //need 10

    ////////////////////////////////////////

    StdVector sv2(local_array, 15);
}