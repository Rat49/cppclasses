#pragma once

#include <cstring>

class SomeString
{
public:
    SomeString()
    {
        m_data = nullptr;
        m_len = 0;
    }

    SomeString(const char* str)
    {
        m_len = strlen(str);
        m_data = new char[m_len + 1];
        m_data[m_len] = 0;
        strncpy(m_data, str, m_len);
    }

    int GetLen() const { return m_len; }

    friend SomeString operator+(const char* left, const SomeString& right);

private:
    int m_len;
    char* m_data;
};

SomeString operator+(const char* left, const SomeString& right)
{
    SomeString result;
    int leftLen = strlen(left);
    result.m_len = leftLen + right.GetLen();
    result.m_data = new char[result.m_len + 1];
    result.m_data[result.m_len] = 0;
    strncpy(result.m_data, left, leftLen);
    strncpy(result.m_data, right.m_data, right.GetLen());
    return result;
}