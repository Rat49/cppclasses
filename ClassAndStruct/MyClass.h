#pragma once

class MyInterface
{
public:
    void JustForFun()
    {
        int a = 1; // 1 is nothing
    }

    virtual ~MyInterface() = 0;
    virtual void VeryImportantMethod() = 0;
};

class MyBase : protected MyInterface
{
public:
    void DoSomeImportantStuff()
    {
        JustForFun();
        VeryImportantMethod();
    }

    void VeryImportantMethod() override final
    {
        int a = 5; // 5 Karl !!!
    }

    ~MyBase() override
    {
    }
};

class MyDerived : private MyBase
{
public:
    void VeryImportantMethod() override
    {
    }

    void NotSoImportantMethod()
    {
        int a = 3; // you can do better
    }
};

class MyClass
{
public:
    void LetsUse()
    {
        MyInterface interface_instance;
        MyBase base_instance;
        MyDerived derived_instance;

        base_instance.JustForFun();

        derived_instance.JustForFun();
        derived_instance.DoSomeImportantStuff();
    }
};
