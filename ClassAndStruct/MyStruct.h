#pragma once

class Class1
{
    int c;
};

struct Struct1
{
    int s;

    static Struct1* CreateStruct();
    static void RemoveStruct(Struct1* s);
};

Struct1* Struct1::CreateStruct()
{
    return new Struct1();
}

void Struct1::RemoveStruct(Struct1* toRemove)
{
    delete toRemove;
}

void SomeFunc()
{
    Class1 theClass;
    theClass.c = 10;

    Struct1 theStruct;
    theStruct.s = 10;

    Struct1* sss = Struct1::CreateStruct();
    Struct1::RemoveStruct(sss);
}
