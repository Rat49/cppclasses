#include <memory>


void main()
{
    float value_on_stack = 0;

    float* value_on_heap = new float(0);

    //change value on stack
    value_on_stack = 42;

    //change value on heap
    *value_on_heap = 42;

    float* pointer_to_value_on_stack = &value_on_stack;

    float** pointer_to_value_on_heap = &value_on_heap;

    float& reference_to_value_on_stack = value_on_stack;

    float& reference_to_value_on_heap = *value_on_heap;

    delete value_on_heap;
    reference_to_value_on_heap = 56; //error: trying to access memory which is no longer owned by application

    {
        float scoped_value = 42;
        reference_to_value_on_stack = scoped_value;
    }

    reference_to_value_on_stack = 56; //error: trying to access variable on stack which is no longer exists
}