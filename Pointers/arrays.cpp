#include <memory>
#include <vector>

void arrays()
{
    ////////////////////////////////////////////////
    //one dimensional array[10]
    float* array1 = new float[10]; //create sequence of 10 elements

    array1[1] = 42; //usage

    delete[] array1; //removal

    ////////////////////////////////////////////////
    //two dimensional array[10][20]
    float** array2 = new float*[10]; //create array(table header) to hold links to other arrays
    for (int i = 0; i < 10; ++i)
    {
        array2[i] = new float[20]; //actually noone stops us to create different size for each column
    }

    array2[1][2] = 42; //usage
    
    for (int i = 0; i < 10; ++i)
    {
        delete[] array2[i]; //remove inner array
    }
    delete[] array2; //remove table header

    ////////////////////////////////////////////////
    //three dimensional array[10][20][30]
    float*** array3 = new float**[10]; //to imagine - this is cube header. each element will hold table
    for (int i = 0; i < 10; ++i)
    {
        array3[i] = new float*[20]; //create table header. you can do it with different sizes, you pervert.

        for (int j = 0; j < 20; ++j)
        {
            array3[i][j] = new float[30]; //create sequence of elements. same could be different size.
        }
    }

    array3[1][2][3] = 42; //usage

    for (int i = 0; i < 10; ++i)
    {
        for (int j = 0; j < 20; ++j)
        {
            delete[] array3[i][j]; //remove inner arrays
        }
        delete[] array3[i]; //remove table header
    }
    delete[] array3; //remove cube header
}

void vectors()
{
    //one dimensional vector[10]
    {
        std::vector<float> array1(10); //create vector on 10 elements

        array1[1] = 42; //use

    }//exit scope -> vector automatically removed

    //two dimensional vector[10][20]
    {
        std::vector< std::vector<float> > array2(10); //create vector of vectors
        for (int i = 0; i < 10; ++i)
        {
            array2[i].resize(20); //could be any size for each inner vector
        }

        array2[1][2] = 42; //usage

    }//exit scope -> vector automatically removed

    //three dimensional vector[10][20][30]
    {
        std::vector< std::vector< std::vector<float> > > array3(10); //create vector of vectors of vectors :)
        for (int i = 0; i < 10; ++i)
        {
            array3[i].resize(20); //could be any size for each inner vector
            for (int j = 0; j < 20; ++j)
            {
                array3[i][j].resize(30); //again could be any size
            }
        }

        array3[1][2][3] = 42; //usage

    }//exit scope -> vector automatically removed
}


void setArray1(float* array) {}

void setArray2(float** array) {}

void setArray3(float*** array) {}


void setConstArray1(const float* const array) {}

void setConstArray2(const float* const* const array) {}

void setConstArray3(const float* const* const* const array) {}


void setVector1(std::vector<float>& array) {}

void setVector2(std::vector< std::vector<float> >& array) {}

void setVector3(std::vector< std::vector< std::vector<float> > >& array) {}


void setConstVector1(const std::vector<float>& array) {}

void setConstVector2(const std::vector< std::vector<float> >& array) {}

void setConstVector3(const std::vector< std::vector< std::vector<float> > >& array) {}
