#include <memory>

class Widget
{
public:
    void Execute() {}
};

void smartPointers()
{
    {
        std::unique_ptr<Widget> on_heap(new Widget); //create and wrap into class

        //use it as it is simple pointer
        (*on_heap).Execute(); 
        on_heap->Execute();

    } //exit of scope -> float_on_heap removed automatically

    {
        std::shared_ptr<Widget> ptr(new Widget());
        ptr->Execute();

        ////////////////////////////////////////////
        std::weak_ptr<Widget> wPtr(ptr);
        if (!wPtr.expired())
        {
            std::shared_ptr<Widget> p = wPtr.lock();
            p->Execute();
        }//unlock wPtr by removement p
    } //ptr destroyed. wptr - expired & destroyed
}