#include <memory>

class Widget
{
public:
    void Execute() {}
};

void classes()
{
    Widget onStack;

    Widget* onHeap = new Widget();

    Widget* ptrOnStack = &onStack;

    Widget** ptrOnPtrOnHeap = &onHeap;

    Widget* copyOfOnHeapPtr = onHeap;

    Widget* dynamicArrayOnHeap = new Widget[10];

    Widget staticArrayOnStack[10];

    Widget& refOnStack = onStack;

    Widget*& refOfPtrOnHeap = onHeap;

    delete onHeap;
    delete[] dynamicArrayOnHeap;

    onStack.Execute(); //still works
    refOnStack.Execute(); //works
    ptrOnStack->Execute(); //works

    onHeap->Execute(); //memory corruption
    (*ptrOnPtrOnHeap)->Execute(); //memory corruption
    copyOfOnHeapPtr->Execute(); //memory corruption
    refOfPtrOnHeap->Execute(); //memory corruption

    onHeap = nullptr;

    onHeap->Execute(); //null pointer exception
}